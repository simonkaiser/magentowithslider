<?php
require_once 'app/Mage.php';
Mage::app();
Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
$importDir = Mage::getBaseDir('media') . DS . 'incoming/';
$file_handle = fopen("images.csv", "r");
while (!feof($file_handle) ) {
	$line_of_text = fgetcsv($file_handle, 1024);
	$productSKU = $line_of_text[0];
	$ourProduct = Mage::getModel('catalog/product')->loadByAttribute('sku',$productSKU);
	$fileName = $line_of_text[1];
	$filePath = $importDir.$fileName;
	if (file_exists($filePath)) {
		$ourProduct->addImageToMediaGallery($filePath, array('image', 'small_image', 'thumbnail'), false, false);
		$ourProduct->save();
	} else {
		echo $productSKU . " not done";
		echo "<br>";
	}	
}
fclose($file_handle);
?>