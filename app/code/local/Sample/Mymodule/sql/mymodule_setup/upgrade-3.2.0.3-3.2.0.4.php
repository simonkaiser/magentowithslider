<?php
 
$installer = $this;
$connection = $installer->getConnection();
 
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('sales/quote'),'delivery_date', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => false,
    'length'    => 255,
    'after'     => null, // column name to insert new column after
    'comment'   => 'Delivery Date'
    )); 

$installer->endSetup();
