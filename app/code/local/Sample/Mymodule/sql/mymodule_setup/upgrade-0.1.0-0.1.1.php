<?php
$installer = $this;  
$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('mymodule2')};
CREATE TABLE {$this->getTable('mymodule2')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->endSetup(); 


?>
