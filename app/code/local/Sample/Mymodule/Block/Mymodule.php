<?php
class Sample_Mymodule_Block_Mymodule extends Mage_Core_Block_Template{
    public function getContent(){
		$model=Mage::getModel("mymodule/mymodule")->load();
		$fetch=$model->getCollection()->setOrder('mymodule_id', 'desc')->getData();
		return $fetch;
    }
    public function __construct(){
        parent::__construct();
        $collection = Mage::getModel("mymodule/mymodule")->getCollection();
        $this->setCollection($collection);
    }
    protected function _prepareLayout(){
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
    public function getPagerHtml(){
        return $this->getChildHtml('pager');
    }
}
?>
