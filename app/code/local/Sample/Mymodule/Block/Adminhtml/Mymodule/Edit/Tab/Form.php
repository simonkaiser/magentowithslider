<?php
  
class Sample_Mymodule_Block_Adminhtml_Mymodule_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('mymodule_form', array('legend'=>Mage::helper('mymodule')->__('Item information')));
        
        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('mymodule')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'title',
        ));
  
        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('mymodule')->__('Status'),
            'name'      => 'status',
            'values'    => array(
                array(
                    'value'     => 1,
                    'label'     => Mage::helper('mymodule')->__('Active'),
                ),
  
                array(
                    'value'     => 0,
                    'label'     => Mage::helper('mymodule')->__('Inactive'),
                ),
            ),
        ));
        
        $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label'     => Mage::helper('mymodule')->__('Content'),
            'title'     => Mage::helper('mymodule')->__('Content'),
            'style'     => 'width:98%; height:400px;',
            'wysiwyg'   => false,
            'required'  => true,
        ));
        
        if ( Mage::getSingleton('adminhtml/session')->getmymoduleData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getmymoduleData());
            Mage::getSingleton('adminhtml/session')->setmymoduleData(null);
        } elseif ( Mage::registry('mymodule_data') ) {
            $form->setValues(Mage::registry('mymodule_data')->getData());
        }
        return parent::_prepareForm();
    }
} 
