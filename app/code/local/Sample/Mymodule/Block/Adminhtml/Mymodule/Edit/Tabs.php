 <?php
  
class Sample_Mymodule_Block_Adminhtml_Mymodule_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  
    public function __construct()
    {
        parent::__construct();
        $this->setId('mymodule_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('mymodule')->__('News Information'));
    }
  
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('mymodule')->__('Item Information'),
            'title'     => Mage::helper('mymodule')->__('Item Information'),
            'content'   => $this->getLayout()->createBlock('mymodule/adminhtml_mymodule_edit_tab_form')->toHtml(),
        ));
        
        return parent::_beforeToHtml();
    }
}
