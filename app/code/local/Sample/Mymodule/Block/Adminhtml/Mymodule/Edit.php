<?php
  
class Sample_Mymodule_Block_Adminhtml_Mymodule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                
        $this->_objectId = 'id';
        $this->_blockGroup = 'mymodule';
        $this->_controller = 'adminhtml_mymodule';
  
        $this->_updateButton('save', 'label', Mage::helper('mymodule')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('mymodule')->__('Delete Item'));
    }
  
    public function getHeaderText()
    {
        if( Mage::registry('mymodule_data') && Mage::registry('mymodule_data')->getId() ) {
            return Mage::helper('mymodule')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('mymodule_data')->getTitle()));
        } else {
            return Mage::helper('mymodule')->__('Add Item');
        }
    }
} 
