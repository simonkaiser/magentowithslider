<?php
echo '<pre>';
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
set_time_limit(0);
ini_set('memory_limit', '1024M');

include_once "app/Mage.php";
include_once "downloader/Maged/Controller.php";
define('MAGENTO', realpath(dirname(__FILE__)));
umask(0);
Mage::app();
Mage::init();
$app = Mage::app('default');
$row = 0;

if (($handle = fopen("products_info.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        echo 'Importing product : '.'<br />';
        foreach($data as $d)
        {
            echo $d.'<br />';
        }
        $num = count($data);
        echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;

        if($row == 1) continue;
        
        $product = Mage::getModel('catalog/product');
        $product->setName($data[0]);
        $product->setDescription($data[1]);
        $product->setShortDescription($data[2]);
        $product->setSku($data[3]);
        
        $test = explode(',', $data[17]);
        $size = explode(',', $data[18]);
        $cnt = $data[19];
        $prodId = array();
        $i=0;
        $sizearr = array('small' => '6','medium' => '7','large' => '8' );
        foreach($test as $v){
            $simple_product = Mage::getModel('catalog/product');
            $setsize = $sizearr[$size[$i]];
            $sku = $v.'-'.$size[$i].'-'.$i;
            if($v=='red'){
                $col_val = '3';
            } else if($v=='black') {
                $col_val = '4';
            } else if($v=='green') {
                $col_val = '5';
            }
            $i++;
            $simple_product->setName($data[0]);
            $simple_product->setSize($setsize);
            $simple_product->setDescription($data[1]);
            $simple_product->setShortDescription($data[2]);
            $simple_product->setSku($data[3]);
            $simple_product->setSku($sku);
            $simple_product->setColor($col_val);
            $simple_product->setWeight($data[4]);
            $simple_product->setNewsFromDate($data[5]);
            $simple_product->setNewsToDate($data[6]); 
            $simple_product->setStatus($data[7]);
            $simple_product->setCountryOfManufacture($data[8]);
            $simple_product->setUrlKey($data[9]);
            $simple_product->setPrice($data[10]);
            $simple_product->setPercentDiscount($data[11]);
            $simple_product->setSpecialPrice($data[12]);
            $simple_product->setSpecialFromDate($data[13]);
            $simple_product->setSpecialToDate($data[14]);
            $simple_product->setTaxClassId($data[15]);
            $simple_product->setTypeId('simple');
            $simple_product->setAttributeSetId(4);
            $simple_product->setCategoryIds(array($categories));
            $simple_product->setVisibility(1); 
            $simple_product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
            //print_r($simple_product->getData());
            try {
                $simple_product->save();
                echo $simple_product->getId();
                $prodId[]=$simple_product->getId();
                $stockItem = Mage::getModel('cataloginventory/stock_item');
                $stockItem->assignProduct($simple_product);
                $stockItem->setData('is_in_stock', 1);
                $stockItem->setData('stock_id', 1);
                $stockItem->setData('store_id', 1);
                $stockItem->setData('manage_stock', 1);
                $stockItem->setData('use_config_manage_stock', 0);
                $stockItem->setData('min_sale_qty', 1);
                $stockItem->setData('use_config_min_sale_qty', 0);
                $stockItem->setData('max_sale_qty', 1000);
                $stockItem->setData('use_config_max_sale_qty', 0);
                $stockItem->setData('qty', 10);
                $stockItem->save();
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }             
        }
        
        
        //~ $product->setWeight($data[4]);
        //~ $product->setNewsFromDate($data[5]); 
        //~ $product->setNewsToDate($data[6]); 
        //~ $product->setStatus($data[7]); 
        //~ $product->setCountryOfManufacture($data[8]);
        //~ $product->setUrlKey($data[9]);
        //~ $product->setPrice($data[10]);
        //~ $product->setPercentDiscount($data[11]);
        //~ $product->setSpecialPrice($data[12]);
        //~ $product->setSpecialFromDate($data[13]);
        //~ $product->setSpecialToDate($data[14]);
        //~ $product->setTaxClassId($data[15]);
        //~ $product->setTypeId('configurable');
        //~ $product->setAttributeSetId(4);
        //~ $product->setCategoryIds(array($categories)); 
        //~ $product->setVisibility(4);
        //~ $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        //~ $product -> getTypeInstance() -> setUsedProductAttributeIds(array(92,132), $product);
        //~ $configurableAttributesData = $product -> getTypeInstance() -> getConfigurableAttributesAsArray($product);
        //~ $product -> setCanSaveConfigurableAttributes(true);
        //~ $product -> setConfigurableAttributesData($configurableAttributesData);
        //~ $product -> setConfigurableProductsData($configurableProductsData);           
        //~ try {
            //~ $product->save();
            //~ echo $product->getId();
            //~ $cat_stock_item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
            //~ if (!$cat_stock_item->getId()) {
                //~ $cat_stock_item->setData('product_id', $product->getId());
                //~ $cat_stock_item->setData('stock_id', 1); 
            //~ }
            //~ $cat_stock_item->setData('is_in_stock', 1);
            //~ $cat_stock_item->setData('manage_stock', 1);
            //~ $cat_stock_item->setData('use_config_manage_stock', 0);
            //~ try {
                //~ $cat_stock_item->save();
            //~ } catch (Exception $e) {
                //~ echo "{$e}";
            //~ }
            //~ Mage::getResourceSingleton('catalog/product_type_configurable')->saveProducts($product, $prodId);
        //~ } catch (Exception $e) {
            //~ echo 'Caught exception: ',  $e->getMessage(), "\n";
        //~ }
    }
    fclose($handle);    
}

?>
