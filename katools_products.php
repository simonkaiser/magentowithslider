<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(0); 
require_once '../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//~ require_once 'reader.php'; 
//~ $excel = new Spreadsheet_Excel_Reader();

//~ $excel->read('products.xls'); 

$row_header=$excel->sheets[0]['cells'][1]; 
//print_r($row_header);die;

	//$excel_labels=array();
	
unset($product);
unset($customInstance);
unset($dropInstance);

	//~ for($x=121;$x<=123;$x++){
		
		 $custom_options = array();
		
//echo $x;
$row=$excel->sheets[0]['cells'][$x];
//print_r($row); die;
//$description = utf8_encode($row[18]);
//echo $description; die;
$product = Mage::getModel('catalog/product');
$product->setWebsiteIds(array(1));
$product->setName($row[17]); 
$product->setYoutubeId($row[2]);
$product->setSku($row[4]);
$product->setTypeId('simple'); 
$product->setAttributeSetId(4);
$product->setWeight($row[10]);
$product->setStatus($row[11]); 
$product->setTaxClassId($row[12]); 
$product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
$product->setManufacturer($row[13]); 
$product->setPrice($row[6]);
$product->setDescription(($row[18]));
$product->setSize($row[22]);
$product->setAmount($row[23]);
$product->setTaxClassId(2); // taxable goods
$product->setShortDescription($row[19]);
$product->setStockData(array(
                'use_config_manage_stock' => 0, 
                'manage_stock' => 1, 
                'is_in_stock' => 1,
				'qty' => 10,
            )
        );

$collection=Mage::getResourceModel('catalog/category_collection')
->addAttributeToFilter('name',['in' => $row[21]])->addAttributeToSelect('*');
foreach ($collection as $category) {
$category_id = $category->getId();
}
$product->setCategoryIds(array($category_id));
//$product->setCategoryIds(array('3'));

//echo $categoryId; die;


// Main Image Code Starts

$mediattribute = array(
    'thumbnail'   ,
    'small_image',
    'image'  
);

$importDir = Mage::getBaseDir('media') . DS . 'katools_images/';

    $filePath = $importDir.$row[5];
    if ( file_exists($filePath) ) {
        try {
            $product->addImageToMediaGallery($filePath,$mediattribute, false, false);
        } catch (Exception $e) {
            echo $e->getMessage('Inserted');
        }
    } else {
        echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
    }
// Main Image Code Ends

// Additional Image Code starts

$imageData=unserialize($row[15]);
//print_r($imageData); die;
$imgArray  = $imageData;   
$importDir = Mage::getBaseDir('media') . DS . 'katools_images/';
foreach($imgArray as $imageType => $fileName) {
    $filePath = $importDir.$fileName;
    if ( file_exists($filePath) ) {
        try {
            $product->addImageToMediaGallery($filePath, $imageType, false, false);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    } else {
        echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
    }
}

// Additional Image Code Ends


//print_r($product->getData()); die;
$product->save();


/*foreach ($product->getOptions() as $o) {
   $o->getValueInstance()->deleteValue($o->getId());
   $o->deletePrices($o->getId());
   $o->deleteTitles($o->getId());
   $o->delete();
}

*/


// Custom Options Code starts for Size
//if(array_key_exists(22, $row) && !is_null($row[22])) {
if($row[22]!='')
{
echo $row[4]. '<br>';
$sizes= array();
$sizes = explode(',', $row[22]);
if(count(array_filter($sizes)) > 0){
$drop = array();
$custom_options = array();

for($i = 0; $i < count($sizes); $i++){
$drop[$i]['is_delete'] = '';
$drop[$i]['title'] = $sizes[$i];
$drop[$i]['price_type'] = 'fixed';
$drop[$i]['price'] = '';
$drop[$i]['sku'] = '';
}
$custom_options[] = array(
                'title' => 'Size',
                'type' => 'drop_down',
                'is_require' => true,
                'sort_order' => 1,
                'values' => $drop
            );
$dropInstance = $product->getOptionInstance()->unsetOptions();

$product->setHasOptions(1);
$dropInstance->addOption($custom_options);
$dropInstance->setProduct($product);

}
}

// Code Ends


//Custom Options Code starts for Amount
//if(array_key_exists(23, $row) && is_null($row[23])) {
if($row[23]!='')
{
echo $row[4]. '<br>';
$amount = array();
$amount = explode(';', $row[23]);
if(count(array_filter($amount)) > 0){
$custom = array();
$custom_options = array();

for($i = 0; $i < count($amount); $i++){
$custom[$i]['is_delete'] = '';
$custom[$i]['title'] = $amount[$i];
$custom[$i]['price_type'] = 'fixed';
$custom[$i]['price'] = '';
$custom[$i]['sku'] = '';
}
$custom_options[] = array(
                'title' => 'Amount',
                'type' => 'drop_down',
                'is_require' => true,
                'sort_order' => 1,
                'values' => $custom
            );
$customInstance = $product->getOptionInstance()->unsetOptions();

$product->setHasOptions(1);
$customInstance->addOption($custom_options);
$customInstance->setProduct($product);

}
}

//Code Ends

if(count($custom_options)) {
   foreach($custom_options as $option) {
      try {
        $opt = Mage::getModel('catalog/product_option');
        $opt->setProduct($product);
        $opt->addOption($option);
        $opt->saveOptions();
      }
      catch (Exception $e) {}
   }
}


//die('Product Created'); 
//unset($product);

  $collection = Mage::getModel('catalog/product')
                    ->getCollection()                   
                    ->addAttributeToSort('created_at', 'desc');   
    $collection->getSelect()->limit(1);
    echo $latestItemId = $collection->getLastItem()->getId();  
    $obj = Mage::getModel('catalog/product');
$product = $obj->load($latestItemId);
//echo $product; 
$product->save();
echo "Inserted";
echo '<br>';   
} 
?>
