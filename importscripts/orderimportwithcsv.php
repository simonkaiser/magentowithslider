<?php
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
$collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*');
$po = rand(1, 999999);
$i=0;
 $result = array();
if (($handle = fopen("orderupload.csv", "r")) != FALSE) {
	while (($data = fgetcsv($handle, 1000, ",")) != FALSE) {
		$row++;
		if($row == 1) continue;
		foreach ($collection as $customer){
		$result[] = $customer->toArray();
		//~ echo $result[0][entity_id];
		$id=$result[0][entity_id]; //get Customer Id
		$customer = Mage::getModel('customer/customer')->load($id);
		$transaction = Mage::getModel('core/resource_transaction');
		$storeId = $customer->getStoreId();
		$reservedOrderId=Mage::getSingleton('eav/config')
		->getEntityType('order')->fetchNewIncrementId($storeId);
		$order = Mage::getModel('sales/order')
		->setIncrementId($reservedOrderId)
		->setStoreId($storeId)
		->setQuoteId(0)
		->setGlobal_currency_code('USD')
		->setBase_currency_code('USD')
		->setStore_currency_code('USD')
		->setOrder_currency_code('USD');
		//Set your store currency USD or any other
		// set Customer data
		$order->setCustomer_email($data[0])
		->setCustomerFirstname($data[1])
		->setCustomerLastname($data[2])
		->setCustomerGroupId($data[3])
		->setCustomer_is_guest(0)
		->setCustomer($customer);
		// set Billing Address
		$billing = Mage::getModel('customer/address')->load($id);
		$billingAddress = Mage::getModel('sales/order_address') ->setStoreId($storeId)
		->setAddressType(Mage_Sales_Model_Quote_Address::
		TYPE_BILLING)
		->setCustomerId($id)
		->setCustomerAddressId()
		->setCustomer_address_id()
		->setPrefix()
		->setFirstname($data[1])
		->setLastname($data[2])
		->setCompany($data[4])
		->setStreet($data[5])
		->setCity($data[6])
		->setCountry_id($data[7])
		->setRegion($data[8])
		->setPostcode($data[9])
		->setTelephone($data[10])
		->setFax($data[11]);
		$order->setBillingAddress($billingAddress);
		$shipping = Mage::getModel('customer/address')->load($id);
		$shippingAddress = Mage::getModel('sales/order_address')
		->setStoreId(1)
		->setAddressType(Mage_Sales_Model_Quote_Address::
		TYPE_SHIPPING)
		->setCustomerId($id)
		->setCustomerAddressId($customer->getDefaultShipping())
		->setCustomer_address_id($shipping->getEntityId())
		->setPrefix()
		->setFirstname($data[1])
		->setLastname($data[2])
		->setCompany($data[4])
		->setStreet($data[12])
		->setCity($data[12]) 
		->setCountry_id($data[7])
		->setRegion($data[13])
		->setPostcode($data[15])
		->setTelephone($data[16])
		->setFax($data[17]);

		//~ Shipping Info..
		$shippingprice = $data[18];
		$order->setShippingAddress($shippingAddress)->setShippingDescription($data[19])->setShippingAmount($shippingprice)->setBaseShippingAmount($shippingprice);
		$orderPayment = Mage::getModel('sales/order_payment')
		->setStoreId($storeId)
		->setCustomerPaymentId(0)
		->setMethod('purchaseorder')
		->setPo_number('PO'.$po);
		$order->setPayment($orderPayment); 
		$subTotal = $shippingprice;
		$products = array(
		//~ '1' => array(
		//~ 'qty' => 2
		//~ ),
		'2' => array(
		'qty' => 1
		)
		); 
		foreach ($products as $productId=>$product)
		{
		$_product=Mage::getModel('catalog/product')->load(19);
		$rowTotal = $_product->getPrice() * $product['qty'];
		$orderItem = Mage::getModel('sales/order_item')
		->setStoreId($storeId)
		->setQuoteItemId(0)
		->setQuoteParentItemId(NULL)
		->setProductId(18)
		->setProductType($_product->getTypeId())
		->setQtyBackordered(NULL)
		->setTotalQtyOrdered($product['rqty'])
		->setQtyOrdered($product['qty'])
		->setName($_product->getName())
		->setSku($_product->getSku())
		->setPrice($_product->getPrice())
		->setBasePrice($_product->getPrice())
		->setOriginalPrice($_product->getPrice())
		->setRowTotal($rowTotal)
		->setBaseRowTotal($rowTotal);
		$subTotal += $rowTotal;
		$order->addItem($orderItem);
		} 
		$order->setSubtotal($subTotal)
		->setBaseSubtotal($subTotal)
		->setGrandTotal($subTotal)
		->setBaseGrandTotal($subTotal);
		$transaction->addObject($order);
		$transaction->addCommitCallback(array($order, 'place'));
		$transaction->addCommitCallback(array($order, 'save'));
		//~ if($transaction->save()){
			//~ echo "Order Success<br>";
		//~ }
		$i++;
		}
	}
}
?> 

