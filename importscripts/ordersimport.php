<?php

require_once '../app/Mage.php';
	umask(0);
	Mage::app('default');
	
	if (($handle = fopen("order_import.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        $row++;
        if($row>1){  
            break;
        }
   else
   {
	
	$customer_email = $data[19]; 
	$customer = Mage::getModel("customer/customer"); 
	$customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
	$customer->loadByEmail($customer_email); 
	//~ echo $customer->getId(); die;
	//~ echo $customer->getFirstName(); die;
    
	$id=$customer->getId(); 
	$customer = Mage::getModel('customer/customer')->load($id);
   $group_id = $customer->getGroupId();
   //echo $group_id;
	$transaction = Mage::getModel('core/resource_transaction');
	$storeId = $customer->getStoreId();
	$reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);

	$order = Mage::getModel('sales/order')
		->setIncrementId($reservedOrderId)
		->setStoreId($storeId)
		->setQuoteId(0)
		->setGlobal_currency_code('USD')
		->setBase_currency_code('USD')
		->setStore_currency_code('USD')
		->setOrder_currency_code('USD');
		//Set your store currency USD or any other

	// set Customer data
		$order->setCustomer_email($customer->getEmail())
		->setCustomerFirstname($customer->getFirstname())
		->setCustomerLastname($customer->getLastname())
		->setCustomerGroupId($customer->getGroupId())
		->setCustomer_is_guest(0)
		->setCustomer($customer);

	// set Billing Address
	$billing = $customer->getDefaultBillingAddress();
	$billingAddress = Mage::getModel('sales/order_address')
		->setStoreId($storeId)
		->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
		->setCustomerId($customer->getId())
		->setCustomerAddressId($customer->getDefaultBilling())
		->setCustomer_address_id($customer->getEntityId())
		->setPrefix($customer->getPrefix())
		->setFirstname($billing->getFirstname())
		->setMiddlename($billing->getMiddlename())
		->setLastname($billing->getLastname())
		->setSuffix($billing->getSuffix())
		->setCompany($billing->getCompany())
		->setStreet($billing->getStreet())
		->setCity($billing->getCity())
		->setCountry_id($billing->getCountryId())
		->setRegion($billing->getRegion())
		->setRegion_id($billing->getRegionId())
		->setPostcode($billing->getPostcode())
		->setTelephone($billing->getTelephone())
		->setFax($billing->getFax());
		$order->setBillingAddress($billingAddress);

	$shipping = $customer->getDefaultShippingAddress();
	$shippingAddress = Mage::getModel('sales/order_address')
		->setStoreId($storeId)
		->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
		->setCustomerId($customer->getId())
		->setCustomerAddressId($customer->getDefaultShipping())
		->setCustomer_address_id($customer->getEntityId())
		->setPrefix($customer->getPrefix())
		->setFirstname($shipping->getFirstname())
		->setMiddlename($shipping->getMiddlename())
		->setLastname($shipping->getLastname())
		->setSuffix($shipping->getSuffix())
		->setCompany($shipping->getCompany())
		->setStreet($shipping->getStreet())
		->setCity($shipping->getCity())
		->setCountry_id($shipping->getCountryId())
		->setRegion($shipping->getRegion())
		->setRegion_id($shipping->getRegionId())
		->setPostcode($shipping->getPostcode())
		->setTelephone($shipping->getTelephone())
		->setFax($shipping->getFax());

	$order->setShippingAddress($shippingAddress)
		->setShipping_method('flatrate_flatrate');
		/*->setShippingDescription($this->getCarrierName('flatrate'));*/
		/*some error i am getting here need to solve further*/

	//you can set your payment method name here as per your need
	$orderPayment = Mage::getModel('sales/order_payment')
		->setStoreId($storeId)
		->setCustomerPaymentId(0)
		->setMethod('purchaseorder')
		->setPo_number(' – ');
		$order->setPayment($orderPayment);

	// let say, we have 2 products
	//check that your products exists
	//need to add code for configurable products if any
		$subTotal = 0;
		$products = array(
			'1' => array(
			'qty' => $data[22]
			)
			
		);
		//$totals = Mage::getSingleton('checkout/cart')->getQuote()->getTotals();
		//$subtotal = $totals["subtotal"]->getValue();
		//echo $subtotal;die();
		
		
$product_sku = $data[20];
  
 $product_id = Mage::getModel("catalog/product")->getIdBySku( $product_sku ); 
 //echo $product_id;
	foreach ($products as $productId=>$product) {
		$_product = Mage::getModel('catalog/product')->load($product_id);
		//echo $_product->getGroupPrice()->load($customer->getGroupId());die();
		$grp_price_arr = $_product->getData('group_price');
		$cust_group_id=$grp_price_arr[0]['cust_group'];
		$cust_group_id_ret=$grp_price_arr[1]['cust_group'];
		if($cust_group_id==5){
		    $cust_group_price=$grp_price_arr[0]['price'];
			//echo $cust_group_price;
			}
		 else{
           	 echo $cust_group_price=$grp_price_arr[1]['price'];	
			 
			} 
			
		//echo $grp_price_arr[group_id]['price'];die;
		$rowTotal = $cust_group_price * $product['qty'];
		//echo $rowTotal;die();
		$orderItem = Mage::getModel('sales/order_item')
			->setStoreId($storeId)
			->setQuoteItemId(0)
			->setQuoteParentItemId(NULL)
			->setProductId($productId)
			->setProductType($_product->getTypeId())
			->setQtyBackordered(NULL)
			->setTotalQtyOrdered($product['rqty'])
			->setQtyOrdered($product['qty'])
			->setName($_product->getName())
			->setSku($_product->getSku())
			->setPrice($_product->getPrice())
			->setBasePrice($_product->getPrice())
			->setOriginalPrice($_product->getPrice())
			->setRowTotal($rowTotal)
			->setBaseRowTotal($rowTotal);
			

	$subTotal += $rowTotal;
	//echo $subTotal;die();
	$order->addItem($orderItem);
	}

	$order->setSubtotal($subTotal)
		->setBaseSubtotal($subTotal)
		->setGrandTotal($subTotal)
		->setBaseGrandTotal($subTotal);

	$transaction->addObject($order);
	$transaction->addCommitCallback(array($order, 'place'));
	$transaction->addCommitCallback(array($order, 'save'));
	$transaction->save();
	$last_order_increment_id = Mage::getModel("sales/order")->getCollection()->getLastItem()->getIncrementId();
	echo $last_order_increment_id;
	 }
	}
    fclose($handle);
}  
?>
