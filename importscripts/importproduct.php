<?php
include_once "app/Mage.php";
	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		$product = Mage::getModel('catalog/product');
			$rand = rand(1, 9999);
				$product
					->setTypeId('simple')
					->setAttributeSetId(4) // default attribute set
					->setSku('example_sku' . $rand) // generate a random SKU
					->setWebsiteIDs(array(1));

	$product
		->setCategoryIds(array(2,3))
		->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
		->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH); // visible in catalog and search

	$product->setStockData(array(
		'use_config_manage_stock' => 0, // use global config?
		'manage_stock'            => 1, // should we manage stock or not?
		'is_in_stock'             => 1,
		'qty'                     => 50,
	));

	$product
		->setName('Test' . $rand) // add string attribute
		->setShortDescription('Description') // add text attribute
		->setPrice(24.50) // set up prices
		->setSpecialPrice(19.99)
		->setTaxClassId(2) // Taxable Goods by default
		->setWeight(87);
		$product->save();

	$images = array(
		'thumbnail'   => '20.jpg',
		'small_image' => '20.jpg',
		'image'       => '20.jpg',
	);

	$dir = Mage::getBaseDir('media') . DS . '../';

	foreach ($images as $imageType => $imageFileName) {
		$path = $dir . $imageFileName;
		if (file_exists($path)) {
			echo "<pre>";
			echo $imageType ."File Updated";
			try {
				$product->addImageToMediaGallery($path, $imageType, false);}
					catch (Exception $e){
						echo $e->getMessage();
			}
		} else {
			echo "Can not find image by path: `{$path}`<br/>";
		}
	}

