<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(0);
ini_set('memory_limit', '1024M');
include_once "app/Mage.php";
include_once "downloader/Maged/Controller.php";

Mage::init();

$app = Mage::app('default');
$row = 0;

if (($handle = fopen("catalog_product.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        echo 'Importing product: '.$data[0].'<br />';
        foreach($data as $d)
        {
            echo $d.'<br />';
        }
        $num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
    
        if($row == 1) continue;
        
           $product = Mage::getModel('catalog/product');
 
        $product->setSku('test100');
        $product->setName('suit');        
        $product->setDescription('Test Product');
        $product->setShortDescription('Test Product');
        $product->setManufacturer('US');
        $product->setPrice(20);
        $product->setTypeId('simple');
        
        $fullpath = 'media/catalog/product/thumb/';
        $ch = curl_init ($data[14]);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $rawdata=curl_exec($ch);
        curl_close ($ch);
        $fullpath = $fullpath.$data[13].'.jpg';
        if(file_exists($fullpath)) {
            unlink($fullpath);
        }
        $fp = fopen($fullpath,'x');
        fwrite($fp, $rawdata);
        fclose($fp);
        $product->addImageToMediaGallery($fullpath, 'thumbnail', false);
        
        $fullpath = 'media/catalog/product/small/';
        $ch = curl_init ($data[15]);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $rawdata=curl_exec($ch);
        curl_close ($ch);
        $fullpath = $fullpath.$data[13].'.jpg';
        if(file_exists($fullpath)) {
            unlink($fullpath);
        }
        $fp = fopen($fullpath,'x');
        fwrite($fp, $rawdata);
        fclose($fp);
        $product->addImageToMediaGallery($fullpath, 'small-image', false);
        
        $fullpath = 'media/catalog/product/high/';
        $ch = curl_init ($data[16]);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $rawdata=curl_exec($ch);
        curl_close ($ch);
        $fullpath = $fullpath.$data[13].'.jpg';
        if(file_exists($fullpath)) {
            unlink($fullpath);
        }
        $fp = fopen($fullpath,'x');
        fwrite($fp, $rawdata);
        fclose($fp);
        $product->addImageToMediaGallery($fullpath, 'image', false);
        
        
        
        $product->setAttributeSetId(4); // need to look this up
        $product->setCategoryIds(array($categories[$data[11]])); // need to look these up
        $product->setWeight(0);
        $product->setTaxClassId(2); // taxable goods
        $product->setVisibility(4); // catalog, search
        $product->setStatus(1); // enabled
        
        // assign product to the default website
        $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
         
        
        $stockData = $product->getStockData();
        $stockData['qty'] = $data[18]; //18
        $stockData['is_in_stock'] = $data[17]=="In Stock"?1:0; //17
        $stockData['manage_stock'] = 1;
        $stockData['use_config_manage_stock'] = 0;
        $product->setStockData($stockData);


        //~ $product->save();    
       
        
    }
    fclose($handle);
    
//~ }


?>
