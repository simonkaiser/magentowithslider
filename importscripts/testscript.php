<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(0); 
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		 $custom_options = array();
$product = Mage::getModel('catalog/product');
$product->setWebsiteIds(array(1));
$product->setName('sampleproduct'); 
$product->setSku('sampleproduct120');
$product->setTypeId('simple'); 
$product->setAttributeSetId(4);
$product->setWeight(15);
$product->setStatus(1); 
$categories = array(3);
$product->setCategoryIds($categories);
//~ $product->setCategoryIds(getCategories(array($record[1])));
$product->setTaxClassId(2); 
$product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
$product->setManufacturer('US'); 
$product->setPrice(25);
$product->setDescription(('sampleproduct'));
$product->setShortDescription('sampleproduct');
$product->setStockData(array(
                'use_config_manage_stock' => 0, 
                'manage_stock' => 1, 
                'is_in_stock' => 1,
				'qty' => 101,
            )
        );
// Main Image Code Starts
$mediattribute = array(
    'thumbnail' => '20.jpg',
    'small_image' => '21.jpg',
    'image' => '22.jpg', 
);
$importDir = Mage::getBaseDir('media') . DS . '../';
foreach($mediattribute as $imageType => $fileName) {
    $filePath = $importDir.$fileName;
    if ( file_exists($filePath) ) {
        try {
            $product->addImageToMediaGallery($filePath,$imageType, true, true);
        } catch (Exception $e) {
            echo $e->getMessage('Inserted');
        }
    } else {
        echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
    }
}
// Main Image Code Ends
$product->save();
?>
