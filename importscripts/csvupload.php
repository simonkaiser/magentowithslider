<?php
//~ error_reporting(E_ALL);
//~ ini_set('display_errors', 1);
set_time_limit(0);
ini_set('memory_limit', '1024M');
include_once "app/Mage.php";
include_once "downloader/Maged/Controller.php";
Mage::init();
$app = Mage::app('default');
$row = 0;

if (($handle = fopen("duplicateproduct.csv", "r")) != FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) != FALSE) {
		$row++;
		if($row == 1) continue;
			$product = Mage::getModel('catalog/product');
			$product->setSku($data[1]);
			$product->setName($data[15]);
			$product->setDescription($data[17]);
			$product->setShortDescription($data[20]);
			$product->setManufacturer($data[11]);
			$product->setPrice($data[2]);
			$product->setTypeId('simple');
			$product->setAttributeSetId(4); 
			$categories = array(3);
			$product->setCategoryIds($categories);
			$product->setWeight($data[8]);
			$product->setTaxClassId($data[10]); 
			$product->setVisibility(4);  
			$product->setStatus($data[9]);  
			$product->setNewsFromDate($data[25]);
			$product->setNewsToDate($data[26]); 
			$product->setCountryOfManufacture($data[27]);
			$product->setSpecialFromDate($data[28]);
			$product->setSpecialToDate($data[29]);
                         
		//~ thumbnail
		$fullpath = 'media/image';
		$fullpath = $fullpath."/".$data[4];
		$product->addImageToMediaGallery($fullpath, 'thumbnail', false);
		
		//~ small-image
		$fullpath = 'media/image';
		$fullpath = $fullpath."/".$data[4];
		$product->addImageToMediaGallery($fullpath, 'small_image', false);
		
		//~ image
		$fullpath = 'media/image';
		$fullpath = $fullpath."/".$data[4];
		$product->addImageToMediaGallery($fullpath, 'image', false);
		
		//~ updating inventory details after product submission
			$product->save();
			//~ echo $product->getId();	
			$prodId[]=$product->getId();
			$stockItem = Mage::getModel('cataloginventory/stock_item');
			$stockItem->assignProduct($product);
			$stockItem->setData('is_in_stock', 1);
			$stockItem->setData('stock_id', 1);
			$stockItem->setData('store_id', 1);
			$stockItem->setData('manage_stock', 1);
			$stockItem->setData('use_config_manage_stock', 0);
			$stockItem->setData('min_sale_qty', 1);
			$stockItem->setData('use_config_min_sale_qty', 0);
			$stockItem->setData('max_sale_qty', 1000);
			$stockItem->setData('use_config_max_sale_qty', 0);
			$stockItem->setData('qty', $data[3]);
			$stockItem->save();
	}
}
?>    



