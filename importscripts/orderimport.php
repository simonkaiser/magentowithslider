<?php
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
$collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*');
$po = rand(1, 999999);
$i=0;
 $result = array();
foreach ($collection as $customer){
$result[] = $customer->toArray();
//~ echo $result[0][entity_id];
$id=$result[0][entity_id]; //get Customer Id
$customer = Mage::getModel('customer/customer')->load($id);
$transaction = Mage::getModel('core/resource_transaction');
$storeId = $customer->getStoreId();
$reservedOrderId=Mage::getSingleton('eav/config')
->getEntityType('order')->fetchNewIncrementId($storeId);
$order = Mage::getModel('sales/order')
->setIncrementId($reservedOrderId)
->setStoreId($storeId)
->setQuoteId(0)
->setGlobal_currency_code('USD')
->setBase_currency_code('USD')
->setStore_currency_code('USD')
->setOrder_currency_code('USD');
//Set your store currency USD or any other
// set Customer data
$order->setCustomer_email('simkaiser21@yahoo.com')
->setCustomerFirstname('Simon')
->setCustomerLastname('Kaiser')
->setCustomerGroupId(1)
->setCustomer_is_guest(0)
->setCustomer($customer);
// set Billing Address
$billing = Mage::getModel('customer/address')->load($id);
$billingAddress = Mage::getModel('sales/order_address') ->setStoreId($storeId)
->setAddressType(Mage_Sales_Model_Quote_Address::
TYPE_BILLING)
->setCustomerId($id)
->setCustomerAddressId()
->setCustomer_address_id()
->setPrefix()
->setFirstname('Simon')
->setMiddlename()
->setLastname('Kaiser')
->setSuffix() 
->setCompany('Grossman')
->setStreet('215 heaven rd')
->setCity('Tampa')
->setCountry_id('US')
->setRegion('Florida')
->setRegion_id()
->setPostcode('21545')
->setTelephone('154565656')
->setFax('5465659894');
$order->setBillingAddress($billingAddress);
$shipping = Mage::getModel('customer/address')->load($id);
$shippingAddress = Mage::getModel('sales/order_address')
->setStoreId(1)
->setAddressType(Mage_Sales_Model_Quote_Address::
TYPE_SHIPPING)
->setCustomerId($id)
->setCustomerAddressId($customer->getDefaultShipping())
->setCustomer_address_id($shipping->getEntityId())
->setPrefix()
->setFirstname('Simon')
->setMiddlename()
->setLastname('Kaiser')
->setSuffix()
->setCompany('Grossman')
->setStreet('Heaven St')
->setCity('Miami') 
->setCountry_id('US')
->setRegion('Florida')
->setRegion_id()
->setPostcode('21545')
->setTelephone('864959787')
->setFax('1254554556');

//~ Shipping Info..
$shippingprice = 5;
$order->setShippingAddress($shippingAddress)->setShippingDescription('Flatrate')->setShippingAmount($shippingprice)->setBaseShippingAmount($shippingprice);


$orderPayment = Mage::getModel('sales/order_payment')
->setStoreId($storeId)
->setCustomerPaymentId(0)
->setMethod('purchaseorder')
->setPo_number('PO'.$po);
$order->setPayment($orderPayment); 
$subTotal = $shippingprice;
$products = array(
//~ '1' => array(
//~ 'qty' => 2
//~ ),
'2' => array(
'qty' => 1
)
); 
foreach ($products as $productId=>$product)
{
$_product=Mage::getModel('catalog/product')->load(18);
$rowTotal = $_product->getPrice() * $product['qty'];
$orderItem = Mage::getModel('sales/order_item')
->setStoreId($storeId)
->setQuoteItemId(0)
->setQuoteParentItemId(NULL)
->setProductId(18)
->setProductType($_product->getTypeId())
->setQtyBackordered(NULL)
->setTotalQtyOrdered($product['rqty'])
->setQtyOrdered($product['qty'])
->setName($_product->getName())
->setSku($_product->getSku())
->setPrice($_product->getPrice())
->setBasePrice($_product->getPrice())
->setOriginalPrice($_product->getPrice())
->setRowTotal($rowTotal)
->setBaseRowTotal($rowTotal);
$subTotal += $rowTotal;
$order->addItem($orderItem);
} 
$order->setSubtotal($subTotal)
->setBaseSubtotal($subTotal)
->setGrandTotal($subTotal)
->setBaseGrandTotal($subTotal);
$transaction->addObject($order);
$transaction->addCommitCallback(array($order, 'place'));
$transaction->addCommitCallback(array($order, 'save'));
//~ if($transaction->save()){
	//~ echo "Order Success";
//~ }
$i++;
}
?> 

