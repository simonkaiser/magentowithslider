<html>
	
  <head>
	  <title>My Amazing Webpage</title>
	  <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
	  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
	<style>
		div.autoplay,img{
			height:300px;
			width:70%;
		}
	</style>
  </head>
  
<body>	
	<div class="autoplay">
		<div><img src="<?php echo Mage::getBaseUrl("media")?>slider/windows.jpg"></div>
		<div><img src="<?php echo Mage::getBaseUrl("media")?>catalog/product/1/1/11.jpg"></div>  
		<div><img src="<?php echo Mage::getBaseUrl("media")?>catalog/product/1/2/12.jpg"></div>  
		<div><img src="../media/slider/windows.jpg"></div>  
		<div><img src="../2.jpg"></div>  
	</div>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="slick/slick.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$(".autoplay").slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 1000,
		});
	});
	</script>

</body>

</html>
