<?php
include_once "app/Mage.php";
//~ include_once "downloader/Maged/Controller.php";

//~ Mage::init();
//~ $app = Mage::app('default');

try {
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$product = Mage::getModel('catalog/product');
$product
    ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
    ->setAttributeSetId(9) //ID of a attribute set named 'default'
    ->setTypeId('simple') //product type
    ->setCreatedAt(strtotime('now')) //product creation time
    ->setSku('testsku17') //SKU
    ->setName('test product17') //product name
    ->setWeight(8.00)
    ->setStatus(1) //product status (1 - enabled, 2 - disabled)
    ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
    ->setManufacturer(28) //manufacturer id
    ->setColor(24)
    ->setNewsFromDate(strtotime('now')) //product set as new from
    ->setNewsToDate('06/30/2017') //product set as new to
    ->setCountryOfManufacture('US') //country of manufacture (2-letter country code)
    ->setPrice(11.22) //price in form 11.22
    ->setCost(22.33) //price in form 11.22
    ->setSpecialPrice(3.44) //special price in form 11.22
    ->setSpecialFromDate(strtotime('now')) //special price from (MM-DD-YYYY)
    ->setSpecialToDate('06/30/2015') //special price to (MM-DD-YYYY)
    ->setMsrpEnabled(1) //enable MAP
    ->setMsrpDisplayActualPriceType(1) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
    ->setMsrp(99.99) //Manufacturer's Suggested Retail Price
    ->setMetaTitle('test meta title')
    ->setMetaKeyword('testproduct')
    ->setMetaDescription('test meta description')
    ->setDescription('This is a long description')
    ->setShortDescription('This is a short description')
    ->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
    ->setStockData(array(
                       'use_config_manage_stock' => 0, //'Use config settings' checkbox
                       'manage_stock'=>1, //manage stock
                       'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
                       'max_sale_qty'=>2, //Maximum Qty Allowed in Shopping Cart
                       'is_in_stock' => 1, //Stock Availability
                       'qty' => 999 //qty
                   )
    )
    ->setCategoryIds(array(1)); //assign product to categories
$product->save();
}catch(Exception $e){
Mage::log($e->getMessage());
}
