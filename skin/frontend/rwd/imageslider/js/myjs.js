jQuery(document).ready(function(){
	jQuery(".autoplay").slick({
		slidesToShow: 4,
		slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 750,
		dots: true,
		rrows: true,
		responsive: [
			{
				breakpoint: 700,
				settings: {
				dots: false,
				arrows: false,
				centerMode: false,
				centerPadding: '40px',
				slidesToShow: 3,
				slidesToScroll: 1
			  }
			},
			{
				breakpoint: 600,
				settings: {
				dots: false,
				arrows: false,
				centerMode: false,
				centerPadding: '40px',
				slidesToShow: 2,
				slidesToScroll: 1
			  }
			},
			{
				breakpoint: 400,
				settings: {
				dots: false,
				arrows: false	,
				centerMode: false,
				centerPadding: '40px',
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		]
	});
	jQuery(".categorycaret").click(function(){
		jQuery("#leftnav-tree-3").toggle();
	});
});

